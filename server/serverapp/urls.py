from django.urls import path
from . import views

urlpatterns = [
        path('', views.index, name='index'),
        path('adduser/<str:userid>/',views.adduser,name='adduser'),
        path('userdisplay/<str:userid>/',views.userdisplay,name='userdisplay'),
        path('addbill/<int:year>/<int:month>/<int:day>/<str:starter_id>/<str:info>/',views.addbill,name='addbill'),
        #normally use addrecordauto instead of addrecord, because the auto method takes care of charges and balance(in participates) auto matically
        path('addrecord/<int:year>/<int:month>/<int:day>/<str:info>/<int:billid>/',views.addrecord,name='addrecord'),
        path('addrecordauto/<int:year>/<int:month>/<int:day>/<str:info>/<int:billid>/<str:pay_wx_id>/<int:pay_amount>/<path:attended_wx_ids>/',views.addrecordauto,name='addrecordauto'),
        path('addcharges/<str:userid>/<int:recordid>/(?P<am>\d+\.\d{2})/',views.addcharges,name='addcharges'),
        path('addparticipates/<str:userid>/<int:billid>/',views.addparticipates,name='addparticipates'),
        path('checkdelete/<str:userid>/<int:billid>/',views.checkdelete,name='checkdelete'),
]
