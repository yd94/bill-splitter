from django.contrib import admin

# Register your models here.

from .models import User,Bill,Record,Charges,Participates

admin.site.register(User)
admin.site.register(Bill)
admin.site.register(Record)
admin.site.register(Charges)
admin.site.register(Participates)
