from django.db import models

# Create your models here.

class User(models.Model):
	wx_id = models.CharField(max_length=25, primary_key=True)
	def __str__(self):
		return self.wx_id

class Bill(models.Model):
	bill_id = models.AutoField(primary_key=True)
	bill_name = models.CharField(max_length=20)
	bill_date = models.DateField()
	participate = models.ManyToManyField(User, through='Participates')
	def __str__(self):
		return self.bill_name

class Record(models.Model):
	record_id = models.AutoField(primary_key=True)
	#pay_wx_id = models.ForeignKey(User, on_delete=models.CASCADE)
	bill = models.ForeignKey(Bill,on_delete=models.CASCADE)
	#pay_amount = models.PositiveIntergerField()
	record_date = models.DateField()
	record_info = models.CharField(max_length=50)
	charge = models.ManyToManyField(User, through='Charges')
	def __str__(self):
		return self.record_info

class Charges(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	record = models.ForeignKey(Record,on_delete=models.CASCADE)
	amount = models.DecimalField(max_digits=10,decimal_places=2)

class Participates(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	bill = models.ForeignKey(Bill, on_delete=models.CASCADE)
	balance = models.DecimalField(max_digits=10,decimal_places=2)
