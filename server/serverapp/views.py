from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.http import Http404
# Create your views here.
from django.http import HttpResponse
from django.http import JsonResponse
from .models import User,Bill,Record,Charges,Participates

def index(request):
    return JsonResponse({'Hello':'world'})

def adduser(request,userid):
	u = User(wx_id=userid)
	u.save()
	return HttpResponse('Saved')

def userdisplay(request,userid):
	ans=''
	u = Participates.objects.filter(user_id=userid)
	if len(u)>0:
		for itr in u:
			ans=ans+str(itr.bill.bill_name)+'|'
		ans=ans[0:-1]
		ans+='/'
		for itr in u:
			ans=ans+str(itr.bill.bill_id)+'|'
		ans=ans[0:-1]
		ans+='/'
		for itr in u:
			ans=ans+str(itr.bill.bill_date)+'|'
		ans=ans[0:-1]
		ans+='/'
	return HttpResponse(ans)

def addbill(request,year,month,day,starter_id,info):
	date=str(year)+'-'+str(month)+'-'+str(day)
	b = Bill(bill_name=info,bill_date=date)
	b.save()
	p = Participates(user=get_object_or_404(User,wx_id=starter_id),bill=b,balance=0)
	p.save()
	return HttpResponse(str(b.bill_id))

def addrecord(request,year,month,day,info,billid):
	date=str(year)+'-'+str(month)+'-'+str(day)
	r = Record(record_date=date,record_info=info,bill=get_object_or_404(Bill,bill_id=billid))
	r.save()
	return HttpResponse(str(r.record_id))

def addcharges(request,userid,recordid,am):
	c = Charges(user=get_object_or_404(User,wx_id=userid),record=get_object_or_404(Record,record_id=recordid),amount=am)
	c.save()
	return HttpResponse('Saved')

def addparticipates(request,userid,billid):
	p = Participates(user=get_object_or_404(User,wx_id=userid),bill=get_object_or_404(Bill,bill_id=billid),balance=0)
	p.save()
	return HttpResponse('Saved')

def addrecordauto(request,year,month,day,info,billid,pay_wx_id,pay_amount,attended_wx_ids):
	#add record
	date=str(year)+'-'+str(month)+'-'+str(day)
	r = Record(record_date=date,record_info=info,bill=get_object_or_404(Bill,bill_id=billid))
	r.save()
	recordid=r.record_id
	#add charges
	id_list=attended_wx_ids.split('/')
	per_amount=pay_amount/(len(id_list)+1)
	for itr in id_list:
		temp=Charges.objects.create(user=get_object_or_404(User,wx_id=itr),record=get_object_or_404(Record,record_id=recordid),amount=-per_amount)
		temp.save()
		#modify participates
		modtemp=get_object_or_404(Participates,user=itr,bill=billid)
		modtemp.balance=modtemp.balance-per_amount
		modtemp.save()

	temp=Charges.objects.create(user=get_object_or_404(User,wx_id=pay_wx_id),record=get_object_or_404(Record,record_id=recordid),amount=pay_amount-per_amount)
	temp.save()
	modtemp=get_object_or_404(Participates,user=pay_wx_id,bill=billid)
	modtemp.balance=modtemp.balance+pay_amount-per_amount
	modtemp.save()
	return HttpResponse('Saved')

def checkdelete(request,userid,billid):
	temp=get_object_or_404(Participates,user=userid,bill=billid)
	return str(temp.balance)