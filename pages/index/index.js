
Page({
  data: {
    //text: "this is a page"
    color: "window",
    hoverStyle: "button-hover",
    modalHidden: true,
    newBillName: "",
    //color: ["red","green","brown"],
    billFocus: false, //focus
    startX: 0,
    billList: [ //You need to store the data from the dataBase here!
      {name: "Travel A", date:"05/03/2017",color: "gold",leftOffset: "left::0rpx;"},
      { name: "Travel B", date: "06/07/2017", color: "gold", leftOffset: "left::0rpx;"},
      { name: "Travel C", date: "10/03/2017", color: "gold", leftOffset: "left::0rpx;"},
    ],
    delBtnWidth: 250 //unit rpx
    //billList:[]
  },
  confirmAdd: function(){
    this.data.billFocus = false;
    let temp = this.data.newBillName;
    console.log(temp);
    if(temp === ""){
      this.setData({billFocus: true});
      return;
    }
    else{
      for(let i in this.data.billList){
        console.log(i);
        if(this.data.billList[i].name === temp){
          wx.showToast({
            title: "bill existed!",
            icon: "loading",
            duration: 300
          });
        this.setData({billFocus: true});
        return;
      }
    }
  }
    let today = new Date();
    let day = today.getDate();
    let month = today.getMonth() + 1;
    let year  = today.getFullYear();
    if(day < 10){
      day = '0' + day;
    }
    if(month < 10){
      month = '0' + month;
    }
    let fullDate = month +"/" + day + "/" + year; 

    let newBillList = this.data.billList;
    //let tempColor = this.data.color[newBillList.length % 3];
    newBillList.push({name: temp, date: fullDate ,color: "gold",leftOffset:"left:0rpx" });
    this.setData({billList:newBillList});
    //send information to dataBase
    //create a new box to represent this bill
    this.setData({newBillName : ""});
    this.setData({modalHidden: true});
  },
  
  cancelBill: function(){
    this.setData({billFocus:false});
    this.setData({modalHidden: true})
    this.data.newBillName = "";
  },
  newBillAdded: function(){
    this.setData({modalHidden: !this.data.modalHidden});
    this.setData({billFocus:true});
  },
  billName: function(e){
    this.data.billFocus = true;
    this.data.newBillName = e.detail.value;
    //this.setData({newBillName : temp});
    //return temp;
  },
  touchS: function(e){
    if(e.touches.length === 1){
      //console.log(this.data.billList[e.currentTarget.dataset.index].leftOffset);
      console.log("touchS");
      this.setData({startX: e.touches[0].clientX}); 
    }
  },
  touchM:function(e){
    if(e.touches.length === 1){
      console.log("touchM");
      let moveX = e.touches[0].clientX;
      let disX = this.data.startX - moveX;
      let curDelBtnWidth = this.data.delBtnWidth;
      let leftOff = 0;
      if(disX === 0 || disX < 0){ //move distance < 0 or = 0 
        leftOff = "left:0rpx";
      }
      else if( disX > 0){
        leftOff = "left:-"+disX+"rpx;";
        if(disX >= curDelBtnWidth){
          leftOff = "left:-" + curDelBtnWidth + "rpx;";
        }
      }
      let index = e.currentTarget.dataset.index;
      //console.log(this.data.billList[index].leftOffset);
      let newList = this.data.billList;
      newList[index].leftOffset = leftOff;
      this.setData({billList:newList});
    }
  },
  touchE: function(e){
    if(e.changedTouches.length == 1){
      console.log("TouchEnd");
      let endX = e.changedTouches[0].clientX;
      let disX = this.data.startX-endX;
      let delBtnWidth = this.data.delBtnWidth;
      let leftOff = (disX > delBtnWidth/2) ? "left:-" + delBtnWidth + "rpx": "0rpx;";
      let index = e.currentTarget.dataset.index;
      let newList = this.data.billList;
      newList[index].leftOffset = leftOff; 
      this.setData({billList: newList});
    }
  },
  delBill: function(e){
    let index = e.currentTarget.dataset.index;
    let newBillList = this.data.billList;
    //console.log(index);
    newBillList.splice(index,1);
    this.setData({billList: newBillList});
  },
  detailBill:function(e){
    let index = e.currentTarget.dataset.index;
    console.log(index);
    let billName = this.data.billList[index].name;
    let URL = "../detail/detail?" + "billName=" + billName;
    wx.navigateTo({
      url: URL
    })
  },
  onLoad: function (options) {
    //initialize
  },
  onReady: function () {
   //request data from dataBase:
   //parameter is userID, then it will return a list with billName and date when it was established
  },
  onShow: function () {
    //???
  },
  onHide: function () {
    //hide
  },
  onUnload: function () {
    //close
  }
})